const fs = require('fs')
const assert = require('assert')

const files = fs.readdirSync('../../Assets/Resources/Photos')

assert(files.length > 0, 'No photos found!')

const parsed = files
  .map(fn => {
    const data = {}

    data.filename = fn

    // data.importance = 2

    const [year, category /*, subject, series */ ] = data.filename.substring(0, data.filename.indexOf('.')).split('_')

    if(category != null) {
      data.category = category
    }
    else {
      console.warn(`${fn} category ${category} is invalid!`)
    }

    data.year = Number.parseInt(year, 10)
    if(Number.isNaN(data.year)) {
      console.warn(`'${fn}' has invalid year!`)
    }

    // if(subject != null) {
    //   data.subject = subject
    // }
    // else {
    //   console.warn(`${fn} subject ${subject} is invalid!`)
    // }

    // const index = Number.parseInt(series, 10)
    // if(Number.isNaN(index)) {
    //   data.index = 0
    // }
    // else {
    //   //Correct 1-indexing
    //   data.index = index - 1
    // }

    return data
  })


  /////////
  //Quick histogram.
  /////////
  const years = {}
  const categories = {}
  for(const d of parsed) {
      if(d.year in years) {
          years[d.year]++
      }
      else {
          years[d.year] = 1
      }

      if(d.category in categories) {
          categories[d.category]++
      }
      else {
          categories[d.category] = 1
      }
  }
  console.dir(years)
  console.dir(categories)
  /////////

fs.writeFileSync('metadata.json', JSON.stringify({ data: parsed }, undefined, 2))
