using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TimelineLayout : IPhotoLayout
{
    Transform linearSegmentsParent;

    Material lineSegmentMat;
    List<Graphic> textElems = new List<Graphic>();

    Waiter asyncTransition;

    readonly Color clearWhite = new Color(1f, 1f, 1f, 0);
    const float TIMELINE_SEGMENT_DISTANCE = 15f;

    int[] timelineIncrements;
    const int START_YEAR = 2017;

    public Waiter OnEnter(bool instant = false)
    {
        var meta = PhotoLayouts.rawMetadata;

        timelineIncrements = Resources.Load<TextAsset>("timelineincrements").text.Split('\n').Select(int.Parse).ToArray();


        if(linearSegmentsParent == null) {
            CreateTimelineSegments(meta);
        }
        linearSegmentsParent.gameObject.SetActive(true);

        if(asyncTransition != null) {
            Object.Destroy(asyncTransition);
        }
        asyncTransition = Waiters.Interpolate(2f, t => {
            var targetColor = Color.Lerp(clearWhite, Color.white, Easing.EaseInOut(t, EaseType.Quad));
            lineSegmentMat.color = targetColor;
//            textElems.ForEach(g => g.color = targetColor);
        });

        var years = timelineIncrements.OrderByDescending(year => year).Where(year => year <= START_YEAR).ToArray();

        var desiredCategories = meta.GroupBy(d => d.category).ToDictionary(g => g.Key);
        var categoryTransforms = PhotoLayouts.Instance.transform.Cast<Transform>().Where(t => desiredCategories.ContainsKey(t.name)).ToDictionary(t => t.name);
        foreach(var cat in desiredCategories) {
            if(!categoryTransforms.ContainsKey(cat.Key)) {
                Debug.LogWarning("Category '" + cat.Key + "' (used by " + cat.Value.Select(d => d.filename).Log(", ") + ") has no matching category!");

                //Band-aid
                meta = meta.Except(cat.Value).ToArray();
            }
        }

        var transitions = new List<TransitionInfo>();
        for(int k = 0; k < years.Length - 1; k++) {
            int year = years[k];
            int nextYear = years[k + 1];

            var categories = meta
                .Where(d => d.year <= year && d.year > nextYear) //dependent on sort order!
                .GroupBy(d => d.category)
                .ToArray();

            int categoryCount = categories.Length;

            for(int i = 0; i < categoryCount; i++) {

                Transform categoryParent = categoryTransforms[categories[i].Key];

//                float angle = 2f * Mathf.PI * i / categoryCount;
//                angle += Mathf.PI * k / 2.3f; //Rotate initial angle by an offset each year
//                Vector3 direction = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0);

                var images = categories[i]
                    .Select(d => new {
                        d.importance,
                        d.subject,
                        d.index,
                        transform = PhotoLayouts.GetPhotoTransform(d.id)
                    })
                    .OrderBy(d => d.subject)
                    .ThenBy(d => d.index)
                    .ToArray();

//                Vector3 positionOffset = direction * 5f;
//                Vector3 positionOffset = direction * (12f + PhotoLayouts.GetScaleFromImportance(images[0].importance));
                Vector3 positionOffset = categoryParent.position.xy();

                for(int j = 0; j < images.Length; j++) {

                float angle = 2f * Mathf.PI * j / images.Length;
                angle += Mathf.PI * k / 2.3f; //Rotate initial angle by an offset each year
                Vector3 direction = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0);

                    float importanceFactor = PhotoLayouts.GetScaleFromImportance(images[j].importance);
//                    positionOffset += direction * (5f + importanceFactor) + (Vector3.forward * j / images.Length * 4.5f);

                    positionOffset += images[j].index == 0
                        ?
                            direction * ((importanceFactor * 2.1f) + 3f)
//                            (Vector3.forward * ((float)j / images.Length) * (TIMELINE_SEGMENT_DISTANCE - 0.5f)) +
                        :
                            direction * importanceFactor * (Random.value > 0.5 ? 1f : -1f) * (1f + (Random.value - 0.5f) * 0.2f) +
                            Vector3.forward * 3f * (Random.value > 0.5 ? 1f : -1f) * (1f + (Random.value - 0.5f) * 0.2f);

                    var extraOffset = Vector3.forward * (Random.value - 0.5f) * 0.05f; // random offset to fix z-fighting

                    var targetPos = (Vector3.forward * TIMELINE_SEGMENT_DISTANCE * k) + positionOffset + extraOffset;
//                    var targetRot = Quaternion.identity;

                    var targetRot = images[j].index == 0 || images[j].importance == 1
                        ? Quaternion.identity
                        : (Random.value < 0.9f ? Quaternion.AngleAxis(90f, Vector3.up) : Quaternion.identity);

                    var targetScale = Vector3.one * importanceFactor;


                    var t = images[j].transform;
                    if(instant) {
                        t.localPosition = targetPos;
                        t.localRotation = targetRot;
                        t.localScale = targetScale;
                    }
                    else {
                        transitions.Add(new TransitionInfo {
                            transform = t,
                            initPos = t.localPosition,
                            initRot = t.localRotation,
                            initScale = t.localScale,
                            targetPos = targetPos,
                            targetRot = targetRot,
                            targetScale = targetScale
                        });
                    }
                }
            }
        }

        if(!instant) {
            return Waiters
                .Interpolate(4f, f => {
                    float eased = Easing.EaseInOut(f, EaseType.Cubic);
                    for(int i = 0; i < transitions.Count; i++) {
                        var t = transitions[i];
                        var trans = t.transform;
                        trans.localPosition = Vector3.Lerp(t.initPos, t.targetPos, eased);
                        trans.localRotation = Quaternion.Lerp(t.initRot, t.targetRot, eased);
                        trans.localScale = Vector3.Lerp(t.initScale, t.targetScale, eased);
                    }
                });
        }
        return null;
    }

    void CreateTimelineSegments(PhotoMetadata[] meta)
    {
        var pl = PhotoLayouts.Instance;

        lineSegmentMat = new Material(pl.linePrefab.GetComponentInChildren<Renderer>().sharedMaterial);
        lineSegmentMat.color = Color.clear;

        linearSegmentsParent = new GameObject("LinearSegments").transform;
        linearSegmentsParent.parent = pl.transform;
        linearSegmentsParent.localPosition = Vector3.zero;

        var years = timelineIncrements.OrderByDescending(year => year).Where(year => year <= START_YEAR).ToArray();

        for(int i = 0; i < years.Length; i++) {
            int year = years[i];

            var lineSegment = Object.Instantiate(pl.linePrefab, linearSegmentsParent);
            lineSegment.localPosition = Vector3.forward * TIMELINE_SEGMENT_DISTANCE * i;
            lineSegment.name = year.ToString();
            lineSegment.GetComponentsInChildren<Renderer>().Each(r => r.sharedMaterial = lineSegmentMat);

            var canvas = Object.Instantiate(pl.textCanvasPrefab, lineSegment);
            canvas.localPosition = Vector3.zero;
            var t = canvas.GetComponentInChildren<Text>();
            t.text = year.ToString();
            textElems.Add(t);
        }

        //Also create timeline segments on other side of spheres
        var afterYears = timelineIncrements.OrderBy(year => year).Where(year => year > START_YEAR).ToArray();
        for(int i = 0; i < afterYears.Length; i++) {
            int year = afterYears[i];

            var lineSegment = Object.Instantiate(pl.linePrefab, linearSegmentsParent);
            lineSegment.localPosition = Vector3.back * (TIMELINE_SEGMENT_DISTANCE * i + 60f);
            lineSegment.localRotation = Quaternion.AngleAxis(180f, Vector3.up);
            lineSegment.name = year.ToString();
            lineSegment.GetComponentsInChildren<Renderer>().Each(r => r.sharedMaterial = lineSegmentMat);

            var canvas = Object.Instantiate(pl.textCanvasPrefab, lineSegment);
            canvas.localPosition = Vector3.zero;
            canvas.localRotation = Quaternion.identity;
            var t = canvas.GetComponentInChildren<Text>();
            t.text = year.ToString();
            textElems.Add(t);
        }
    }

    public void OnTick()
    {
        
    }

    public Waiter OnExit(bool instant = false)
    {
        // We don't need to wait to perform this transition so we don't return it,
        // but we will keep track of it and delete it to avoid race conditions
        if(asyncTransition != null) {
            Object.Destroy(asyncTransition);
        }
        asyncTransition = Waiters.Interpolate(2f, t => {
            var targetColor = Color.Lerp(Color.white, clearWhite, Easing.EaseInOut(t, EaseType.Quad));
            lineSegmentMat.color = targetColor;
//            textElems.ForEach(g => g.color = targetColor);
        }).Then(() => linearSegmentsParent.gameObject.SetActive(false));

        if(instant) {
            return null;
        }
        return Waiters.Wait(0);
    }
}
