using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RadialLayout : IPhotoLayout
{
    public Waiter OnEnter(bool instant = false)
    {
        var meta = PhotoLayouts.rawMetadata;

        int min = meta.Min(d => d.year);
        int max = meta.Max(d => d.year);

        var years = Enumerable
            .Range(min, max - min + 1)
            .Reverse()
            .ToArray();

        var categories = meta
            .GroupBy(d => d.category)
            .ToArray();

        int categoryCount = categories.Length;

        var bounding = GameObject.Find("BoundingSphere").transform;
        var photosOrigin = GameObject.Find("Photos").transform;

        var transitions = new List<TransitionInfo>();
        for(int k = 0; k < years.Length; k++) {
            int year = years[k];

            for(int i = 0; i < categoryCount; i++) {
                float angle = 2f * Mathf.PI * i / categoryCount;

                Vector3 direction = new Vector3(Mathf.Sin(angle), 0, Mathf.Cos(angle));

                var images = categories[i]
                    .Where(d => d.year == year)
                    .Select(d => new {
                        d.importance,
                        d.subject,
                        d.index,
                        transform = PhotoLayouts.GetPhotoTransform(d.id)
                    })
                    .OrderBy(d => d.subject)
                    .ThenBy(d => d.index)
                    .ToArray();

                // Offset back from the "photos" parent transform, then back towards the center of the photospheres
                Vector3 positionOffset = -photosOrigin.localPosition + bounding.localPosition /*+ Vector3.up * 10f*/ + direction * bounding.localScale.x / 2f;

                for(int j = 0; j < images.Length; j++) {
                    float importanceFactor = PhotoLayouts.GetScaleFromImportance(images[j].importance);
//                    positionOffset += Vector3.up * (5f + importanceFactor) + (Vector3.forward * j / images.Length * 4.5f);

                    positionOffset += images[j].index == 0
                        ?
                            direction * ((importanceFactor * 2f) + 1f)
//                            (Vector3.forward * ((float)j / images.Length) * (TIMELINE_SEGMENT_DISTANCE - 0.5f)) +
                        :
                            Vector3.Cross(direction, Vector3.up) * importanceFactor * (Random.value > 0.5 ? 1f : -1f) * (1f + (Random.value - 0.5f) * 0.2f) +
                            Vector3.Cross(direction, Vector3.up) * 3f * (Random.value > 0.5 ? 1f : -1f) * (1f + (Random.value - 0.5f) * 0.2f);

                    var extraOffset = images[j].index == 0
                        ? Vector3.zero
                        : Vector3.up * Random.Range(-20f, 20f);

                    extraOffset += direction * (Random.value - 0.5f) * 0.05f; // random offset to fix z-fighting

                    var targetPos = (direction * 5f * k) + positionOffset + extraOffset;
                    var targetRot = Quaternion.LookRotation(direction);
                    var targetScale = Vector3.one * importanceFactor;

                    var t = images[j].transform;
                    if(instant) {
                        t.localPosition = targetPos;
                        t.localRotation = targetRot;
                        t.localScale = targetScale;
                    }
                    else {
                        transitions.Add(new TransitionInfo {
                            transform = t,
                            initPos = t.localPosition,
                            initRot = t.localRotation,
                            initScale = t.localScale,
                            targetPos = targetPos,
                            targetRot = targetRot,
                            targetScale = targetScale
                        });
                    }
                }
            }
        }

        if(!instant) {
            return Waiters
                .Interpolate(4f, f => {
                    float eased = Easing.EaseInOut(f, EaseType.Cubic);
                    for(int i = 0; i < transitions.Count; i++) {
                        var t = transitions[i];
                        var trans = t.transform;
                        trans.localPosition = Vector3.Lerp(t.initPos, t.targetPos, eased);
                        trans.localRotation = Quaternion.Lerp(t.initRot, t.targetRot, eased);
                        trans.localScale = Vector3.Lerp(t.initScale, t.targetScale, eased);
                    }
                });
        }

        return null;
    }

    public void OnTick()
    {

    }

    public Waiter OnExit(bool instant = false)
    {
        if(instant) {
            return null;
        }
        return Waiters.Wait(0);
    }
}
