using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine.Assertions;

public class CameraNodeTarget : MonoBehaviour
{
    [NonSerialized]
    public CameraNode targetNode;
}
