using System;
using System.IO;
using System.Linq;
using UnityEngine;

public class Metadata : MonoBehaviour
{
    PhotoMetadata[] data;

    public PhotoMetadata[] Data
    {
        get
        {
            if(data == null) {
                var photos = Resources.LoadAll<Sprite>("Photos")
                    .Concat(Resources.LoadAll<Sprite>("Transparent"));
                data = photos.Select(p => {
                    var tokens = p.name.Split('_');
                    int year;
                    if(!int.TryParse(tokens[0], out year)) {
                      Debug.LogWarning("Couldn't parse year for file '" + p.name + "'! Defaulting to 2000.");
                      year = 2000;
                    }
                    return new PhotoMetadata {
                        filename = p.name,
                        id = p.name,
                        year = year,
                        category = tokens[1]
                    };
                }).ToArray();
            }
            return data;
        }
    }
}
