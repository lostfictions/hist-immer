using UnityEngine;

using RenderHeads.Media.AVProVideo;
using UnityEngine.Assertions;

public class VideoPlayer : MonoBehaviour
{
    public AnimationCurve volumeFalloff;
    public AnimationCurve ambientVolumeFalloff;

    public float triggerDistance = 20f;
    public float heightThreshold = 10f;

    MediaPlayer videoPlayer;
    Transform cam;

    AudioSource audioSource;

    AudioSource[] otherSources = new AudioSource[0];

    void Awake()
    {
        videoPlayer = GetComponent<MediaPlayer>();
        Assert.IsNotNull(videoPlayer);

        audioSource = GetComponent<AudioSource>();
        Assert.IsNotNull(audioSource);

        cam = Camera.main.transform;

        Waiters.Wait(2f, gameObject).Then(() => otherSources = FindObjectsOfType<AudioSource>());
    }

    void Update()
    {
        var dist = Vector3.Distance(transform.position, cam.position);

        if(dist < triggerDistance && Mathf.Abs(cam.position.y - transform.position.y) < heightThreshold) {
            videoPlayer.Play();
        }
        else {
            videoPlayer.Rewind(true);
            //videoPlayer.Pause();
        }

        videoPlayer.Control.SetVolume(volumeFalloff.Evaluate(dist));
//        audioSource.volume = ambientVolumeFalloff.Evaluate(dist);
        foreach(var s in otherSources) {
            s.volume = ambientVolumeFalloff.Evaluate(dist);
        }
    }
}
