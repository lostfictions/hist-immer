using System.Collections.Generic;
using UnityEngine;

public class CycleSkybox : MonoBehaviour
{
    public Cubemap[] cubemaps;
    public float cycleSpeed = 2.5f;

    Material skybox;
    int currentIndex;
    float currentTimeout;

	void Awake()
	{
	    skybox = new Material(RenderSettings.skybox);
	    RenderSettings.skybox = skybox;
	    currentTimeout = cycleSpeed;
	}
	
	void Update()
	{
	    currentTimeout -= Time.deltaTime;

	    if(currentTimeout < 0) {
	        currentIndex += 1;
	        if(currentIndex == cubemaps.Length) {
	            currentIndex = 0;
	        }

	        currentTimeout = cycleSpeed;
            skybox.SetTexture("_TexA", cubemaps[currentIndex]);
            skybox.SetTexture("_TexB", (currentIndex < cubemaps.Length - 1) ? cubemaps[currentIndex + 1] : cubemaps[0]);
	    }

        skybox.SetFloat("_value", Mathf.Clamp01((cycleSpeed - currentTimeout) / cycleSpeed));
	}
}
