using UnityEngine;
using UnityEngine.Assertions;

public class AnimateSpheremap : MonoBehaviour
{
    public Cubemap[] cubemaps;
    public float cycleSpeed = 2.5f;
    public float waitTime = 2f;

    Material mat;
    int currentIndex;
    float currentCycleTimeout;
    float currentWaitTime;

    CameraNode parentNode;
    FlyCam cam;

	void Awake()
	{
	    var ren = GetComponent<Renderer>();
        mat = new Material(ren.sharedMaterial);
        ren.sharedMaterial = mat;
	    currentCycleTimeout = cycleSpeed;
	    currentWaitTime = waitTime;

	    parentNode = GetComponentInParent<CameraNode>();
        Assert.IsNotNull(parentNode);
	    cam = FindObjectOfType<FlyCam>();
        Assert.IsNotNull(cam);
	}

    void Update()
    {
        // This does some unnecessary work every frame and will cause an unpleasant
        // pop when we go to leave the sphere, but it's what we've got time to do now
        if(cam.CurrentCameraNode != parentNode) {
            currentIndex = 0;
            currentCycleTimeout = cycleSpeed;
            currentWaitTime = waitTime;
            mat.SetTexture("_TexA", cubemaps[0]);
            mat.SetTexture("_TexB", cubemaps[1]);
            mat.SetFloat("_value", 0);
            return;
        }

        if(currentWaitTime >= 0) {
            currentWaitTime -= Time.deltaTime;
        }
        else {
            currentCycleTimeout -= Time.deltaTime;

            if(currentCycleTimeout < 0) {
                currentIndex += 1;
                if(currentIndex == cubemaps.Length) {
                    currentIndex = 0;
                }

                currentCycleTimeout = cycleSpeed;
                currentWaitTime = waitTime;
                mat.SetTexture("_TexA", cubemaps[currentIndex]);
                mat.SetTexture("_TexB", (currentIndex < cubemaps.Length - 1) ? cubemaps[currentIndex + 1] : cubemaps[0]);
            }

            mat.SetFloat("_value", Mathf.Clamp01((cycleSpeed - currentCycleTimeout) / cycleSpeed));
        }
    }
}
