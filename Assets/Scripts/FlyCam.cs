using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Assertions;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class FlyCam : MonoBehaviour
{
    public bool domeEnabled;
    public bool freeFlyEnabled;
    public bool useHmdRotation;
    public bool useMouseOrControllerInput;
    public bool usePointerDirectionWithHmd;

    public GameObject domeRig;

    public Transform calibrationIndicator;

    public CameraNode initialCameraNode;

    public float turnSensitivity = 90;
    public float baseMoveSpeed = 10;
    public float slowMovementMultiplier = 0.25f;
    public float fastMovementMultiplier = 3;

    public float yMax = 80f;
    public float yMin = -80f;

    public float cameraIdleDriftSpeed = 2f;
    

    float rotationX;
    float rotationY;

    public CameraNode CurrentCameraNode { get; private set; }
    Waiter cameraTransition;

    WebSocketPointer pointer;
    GameObject cursor;

    int layerMask;

    bool pendingTap;
    bool pendingCalibrate;
    bool pendingHome;
    
    float calibratedPointerAngleOffset;

    bool isCalibrating;


    void OnEnable()
    {
        layerMask = 1 << LayerMask.NameToLayer("CameraNodeTarget");

        //Delay by a frame to avoid setup race condition
        Waiters.Wait(0, gameObject).Then(() => SetNode(initialCameraNode));
        
        pointer = GetComponent<WebSocketPointer>();
        Assert.IsNotNull(pointer);

        if(pointer.disable) {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else {
            // Can only do raycasts/unity work on main thread, so just set a flag
            //FIXME: cleanup on disable
            pointer.OnTap += () => pendingTap = true;
            pointer.OnCalibrate += () => pendingCalibrate = true;
            pointer.OnHome += () => pendingHome = true;

            Assert.IsNotNull(calibrationIndicator);
        }

        if(domeEnabled) {
            GetComponent<Camera>().enabled = false;
            domeRig.SetActive(true);

            cursor = transform.Find("Cursor").gameObject;
            cursor.SetActive(true);
        }
    }
    
    void Update()
    {
        if(!pointer.disable) {
            WebSocketUpdate();
        }
        else if(useMouseOrControllerInput) {
            ManualUpdate();
        }

        if(!useHmdRotation) {
            transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
            transform.localRotation *= Quaternion.AngleAxis(rotationY, Vector3.left);
        }
    }

    void WebSocketUpdate() {
        rotationX = pointer.Orientation.x * -1f + calibratedPointerAngleOffset;
        rotationY = Mathf.Clamp(pointer.Orientation.y, yMin, yMax);

        if(freeFlyEnabled && cameraTransition == null) {
            if(Mathf.Approximately(pointer.MovementDelta, 0)) {
                if(CurrentCameraNode != null) {
                    //Float towards center of sphere
                    transform.position = Vector3.MoveTowards(
                        transform.position,
                        CurrentCameraNode.transform.position,
                        cameraIdleDriftSpeed * Time.deltaTime
                    );
                }
            }
            else {
                float normalizedDelta = pointer.MovementDelta / 200f; //between -1 and 1
                float movementSpeed = Mathf.Pow(normalizedDelta * 2.5f, 3f);

                Vector3 pos = transform.localPosition;
                if(useHmdRotation) {
                    if(usePointerDirectionWithHmd) {
                        var rot = Quaternion.AngleAxis(rotationX, Vector3.up) * Quaternion.AngleAxis(rotationY, Vector3.left);
                        pos += rot * Vector3.forward * movementSpeed * Time.deltaTime;
                    }
                    else {
                        pos += transform.GetChild(0).forward * movementSpeed * Time.deltaTime;
                    }
                }
                else {
                    pos += transform.forward * movementSpeed * Time.deltaTime;
                }
                transform.localPosition = pos;
            }
        }

        if(pendingTap) {
            pendingTap = false;
            DoTap();
        }

        if(pendingHome) {
            pendingHome = false;
            SetNode(initialCameraNode);
        }

        if(pendingCalibrate) {
            pendingCalibrate = false;

            isCalibrating = !isCalibrating;
            Debug.Log("Calibrating: " + isCalibrating);
            if(isCalibrating) {
                calibratedPointerAngleOffset = 0;
                calibrationIndicator.gameObject.SetActive(true);
                if(cursor != null) {
                    cursor.SetActive(false);
                }
            }
            else {
                calibratedPointerAngleOffset = -transform.localEulerAngles.y;
                calibrationIndicator.gameObject.SetActive(false);
                if(cursor != null) {
                    cursor.SetActive(true);
                }
            }
        }

        if(isCalibrating) {
            calibrationIndicator.position = transform.position + Vector3.forward * 4.5f;
            calibrationIndicator.LookAt(transform.position);
        }
    }

    void ManualUpdate() {
        rotationX += Input.GetAxis("Horizontal Look") * turnSensitivity * Time.deltaTime;
        rotationY += Input.GetAxis("Vertical Look") * turnSensitivity * Time.deltaTime;
        rotationY = Mathf.Clamp(rotationY, yMin, yMax);

        if(Input.GetButtonDown("Back")) {
            freeFlyEnabled = !freeFlyEnabled;
        }

        if(freeFlyEnabled && cameraTransition == null) {
            float movementDelta = Mathf.Abs(Input.GetAxis("Vertical")) + Mathf.Abs(Input.GetAxis("Horizontal")) + Mathf.Abs(Input.GetAxis("UpDown"));
            if(Mathf.Approximately(movementDelta, 0)) {
                if(CurrentCameraNode != null) {
                    //Float towards center of sphere
                    transform.position = Vector3.MoveTowards(
                        transform.position,
                        CurrentCameraNode.transform.position,
                        cameraIdleDriftSpeed * Time.deltaTime
                    );
                }
            }
            else {
                float s = Input.GetAxis("Speed Multiplier");
                float movementSpeed = baseMoveSpeed * (s < 0 ? Mathf.Lerp(1f, slowMovementMultiplier, -s) : Mathf.Lerp(1f, fastMovementMultiplier, s));

                Vector3 pos = transform.localPosition;
                if (useHmdRotation)
                {
                    var hmdTransform = transform.GetChild(0);
                    pos += hmdTransform.forward * movementSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
                    pos += hmdTransform.right * movementSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
                    pos += hmdTransform.up * movementSpeed * Input.GetAxis("UpDown") * Time.deltaTime;
                }
                else
                {
                    pos += transform.forward * movementSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
                    pos += transform.right * movementSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
                    pos += transform.up * movementSpeed * Input.GetAxis("UpDown") * Time.deltaTime;
                }
                transform.localPosition = pos;
            }
            
        }

        if(Input.GetKeyDown(KeyCode.BackQuote)) {
            Cursor.lockState = (Cursor.lockState == CursorLockMode.Locked) ? CursorLockMode.None : CursorLockMode.Locked;
        }

        if(Input.GetButtonDown("Fire1")) {
            DoTap();
        }
    }

    void DoTap() {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward, out hit, 5f, layerMask, QueryTriggerInteraction.Collide)) {
            var nt = hit.transform.GetComponent<CameraNodeTarget>();
            Assert.IsNotNull(nt);
            if(cameraTransition == null) {
                SetNode(nt.targetNode);
            }
        }
    }

    public void SetNode(CameraNode node, bool instantaneous = false)
    {
        if(node == CurrentCameraNode) {
            return;
        }

        if(cameraTransition != null) {
            return;
        }
        if(node != null) {
            if(instantaneous) {
                transform.position = node.transform.position;
            }
            else {
                var start = transform.position;
                var end = node.transform.position;
                cameraTransition = Waiters.Interpolate(1.3f, t => transform.position = Vector3.Lerp(start, end, Easing.EaseInOut(t, EaseType.Quad)), gameObject);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        var node = other.GetComponent<CameraNode>();
        if(node == null) {
            return;
        }

        CurrentCameraNode = node;
        node.OnNodeEnter();
    }

    void OnTriggerExit(Collider other)
    {
        var node = other.GetComponent<CameraNode>();
        if(node == null) {
            return;
        }

        CurrentCameraNode = null;
        node.OnNodeExit();
    }

    void OnTriggerStay(Collider other)
    {
        var node = other.GetComponent<CameraNode>();
        if(node == null) {
            return;
        }

        CurrentCameraNode = node;
    }
}
