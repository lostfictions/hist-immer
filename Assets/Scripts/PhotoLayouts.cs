using System.IO;
using System.Linq;
using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using UnityEngine.Assertions;

public class PhotoLayouts : MonoBehaviour
{
    public AudioData[] audioData = new AudioData[0];

    public bool useMouseClicksToSwitchLayouts = false;

    public bool usePreparsedMetadata = false;
    public TextAsset metadataJson;

    [Header("Prefabs")]
    public Transform textCanvasPrefab;
    public Transform linePrefab;
    public Transform spritePrefab;
    public AudioSource photoAudioSourcePrefab;

    public static PhotoMetadata[] rawMetadata;
    public static Dictionary<string, Transform> imageTransforms;

    static Dictionary<string, AudioClip> photoIdToClip = new Dictionary<string, AudioClip>();
    
    Transform photosParent;

    PhotoLayoutMachine stateMachine;

    PhotoLayoutMachine.Layouts? pendingLayout;

    static PhotoLayouts instance;
    public static PhotoLayouts Instance
    {
        get
        {
            if(instance == null) {
                instance = FindObjectOfType<PhotoLayouts>();
            }
            if(instance == null) {
                throw new Exception("No instantiated " + typeof(PhotoLayouts).FullName);
            }
            return instance;
        }
    }

    public static Transform GetPhotoTransform(string id, bool asThumbnail = false)
    {
        Transform t;
        if(!imageTransforms.TryGetValue(id, out t)) {
            var sprite = Resources.Load<Sprite>("Photos/" + (asThumbnail ? "thumb_" + id : id));

            bool isTransparent = false;
            if(sprite == null) {
                sprite = Resources.Load<Sprite>("Transparent/" + (asThumbnail ? "thumb_" + id : id));
                isTransparent = true;
            }

            Assert.IsNotNull(sprite, "No sprite at 'Photos/' or 'Transparent/' named '" + (asThumbnail ? "thumb_" : "") + id + "'");

            t = Instantiate(Instance.spritePrefab);
            t.parent = Instance.photosParent;
            
            var s = t.GetComponentInChildren<SpriteRenderer>();
            s.sprite = sprite;

            var ct = s.transform;

            float x = s.sprite.bounds.size.x;
            float y = s.sprite.bounds.size.y;

            if(!isTransparent) {
                if(x > 5f || y > 5f) {
                    float factor = x > y ? x / 5f : y / 5f;
                    ct.localScale = Vector3.one / factor;
                }
            }
            else {
                if(x > 10f || y > 10f) {
                    float factor = x > y ? x / 10f : y / 10f;
                    ct.localScale = Vector3.one / factor;
                }
            }

            imageTransforms.Add(id, t);

            AudioClip clip;
            if(photoIdToClip.TryGetValue(id, out clip)) {
                var source = Instantiate(Instance.photoAudioSourcePrefab);
                source.transform.SetParent(t, false);
                source.transform.localPosition = Vector3.zero; 
                source.clip = clip;
                source.Play();
            }
        }
        return t;
    }

    public static float GetScaleFromImportance(int importance)
    {
        if(importance == 1) {
            return 3f;
        }
        if(importance == 3) {
            return 0.5f;
        }
        return 1f;
    }

    void Awake()
    {
        instance = this;

        photoIdToClip = audioData.ToDictionary(d => d.imageId, d => d.clip);
    }

    void Start()
    {
        if(usePreparsedMetadata) {
            rawMetadata = JsonUtility.FromJson<PhotoMetadataSet>(metadataJson.text).data;
        }
        else {
            rawMetadata = GetComponent<Metadata>().Data;
        }

        imageTransforms = new Dictionary<string, Transform>(rawMetadata.Length);

        var pointer = FindObjectOfType<WebSocketPointer>();
        if(pointer != null && !pointer.disable) {
            pointer.OnLayout += layout => pendingLayout = layout;
        }

        photosParent = new GameObject("Photos").transform;
        photosParent.parent = transform;
        photosParent.localPosition = Vector3.zero;

        stateMachine = new PhotoLayoutMachine(PhotoLayoutMachine.Layouts.Timeline);
    }


    
    void Update()
    {
        stateMachine.Tick();

        if(pendingLayout.HasValue) {
            stateMachine.SetLayout(pendingLayout.Value);
            pendingLayout = null;
        }

        if(Input.GetKeyDown(KeyCode.Alpha1)) {
            stateMachine.SetLayout(PhotoLayoutMachine.Layouts.Timeline);
        }
        else if(Input.GetKeyDown(KeyCode.Alpha2)) {
            stateMachine.SetLayout(PhotoLayoutMachine.Layouts.Radial);
        }
        else if(Input.GetKeyDown(KeyCode.Alpha3)) {
            stateMachine.SetLayout(PhotoLayoutMachine.Layouts.Gallery);
        }

        if(useMouseClicksToSwitchLayouts && Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) {
            if(stateMachine.NextLayoutType.HasValue) {
                switch(stateMachine.NextLayoutType.Value) {
                    case PhotoLayoutMachine.Layouts.Timeline:
                        stateMachine.SetLayout(PhotoLayoutMachine.Layouts.Radial);
                        break;
                    case PhotoLayoutMachine.Layouts.Radial:
                        stateMachine.SetLayout(PhotoLayoutMachine.Layouts.Gallery);
                        break;
                    case PhotoLayoutMachine.Layouts.Gallery:
                        stateMachine.SetLayout(PhotoLayoutMachine.Layouts.Timeline);
                        break;
                }
            }
            else {
                switch(stateMachine.CurrentLayoutType) {
                    case PhotoLayoutMachine.Layouts.Timeline:
                        stateMachine.SetLayout(PhotoLayoutMachine.Layouts.Radial);
                        break;
                    case PhotoLayoutMachine.Layouts.Radial:
                        stateMachine.SetLayout(PhotoLayoutMachine.Layouts.Gallery);
                        break;
                    case PhotoLayoutMachine.Layouts.Gallery:
                        stateMachine.SetLayout(PhotoLayoutMachine.Layouts.Timeline);
                        break;
                }
            }
        }
    }
}


internal struct TransitionInfo
{
    public Transform transform;
    public Vector3 initPos;
    public Quaternion initRot;
    public Vector3 initScale;
    public Vector3 targetPos;
    public Quaternion targetRot;
    public Vector3 targetScale;
}

/// For deserializing from JSON only -- can't be an unnamed array.
[Serializable]
public class PhotoMetadataSet
{
    public PhotoMetadata[] data;
}

[Serializable]
public class PhotoMetadata
{
    public string id;
    public string filename;

    public int year;
    public int importance;
    public string category;

    public string subject;
    public int index;
}

public class PhotoLayoutMachine
{
    /// yes, this is not good oop. shhh. none of this is. this needs to get done.
    public enum Layouts { Timeline, Radial, Gallery }

    public Layouts CurrentLayoutType { get; private set; }
    public Layouts? NextLayoutType { get; private set; }

    public IPhotoLayout CurrentLayout { get; private set; }


    Dictionary<Layouts, IPhotoLayout> layouts = new Dictionary<Layouts, IPhotoLayout>();
    Waiter transition;

    public PhotoLayoutMachine(Layouts initialLayout)
    {
        layouts[Layouts.Timeline] = new TimelineLayout();
        layouts[Layouts.Radial] = new RadialLayout();
        layouts[Layouts.Gallery] = new GalleryLayout();

        CurrentLayoutType = initialLayout;
        CurrentLayout = layouts[CurrentLayoutType];
        CurrentLayout.OnEnter(true);
    }

    public void Tick()
    {
        if(transition == null) {
            CurrentLayout.OnTick();
        }
    }

    public void SetLayout(Layouts layout, bool instant = false)
    {
        if(CurrentLayoutType == layout) {
            return;
        }
        IPhotoLayout nextLayout = layouts[layout];
        NextLayoutType = layout;

        if(transition != null) {
            Object.Destroy(transition);
        }
        
        if(instant) {
            CurrentLayout.OnExit(true);
            CurrentLayoutType = layout;
            CurrentLayout = nextLayout;
            NextLayoutType = null;
            CurrentLayout.OnEnter(true);
        }
        else {
            transition = CurrentLayout.OnExit();
            transition.Then(() => {
                CurrentLayoutType = layout;
                CurrentLayout = nextLayout;
                NextLayoutType = null;
                transition = CurrentLayout.OnEnter();
            });
        }
    }
}

public interface IPhotoLayout {
    Waiter OnEnter(bool instant = false);
    void OnTick();
    Waiter OnExit(bool instant = false);
}

[Serializable]
public class AudioData
{
    public string imageId;
    public AudioClip clip;
}
