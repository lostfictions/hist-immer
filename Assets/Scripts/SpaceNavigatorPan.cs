using UnityEngine;
using SpaceNavigatorDriver;

public class SpaceNavigatorPan : MonoBehaviour
{
    public bool lockCursor;

    public float accelerationRate = 0.005f;
    public float accelerationThreshold = 0.15f;
    public float speedModifier = 1f;

    public float deadZone = 0.05f;
	public bool doRotate = false;

    float accel = 1f;

    void Start()
    {
        if(lockCursor) {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

	void Update()
	{
	    Vector3 translation = SpaceNavigator.Translation;
	    if(translation.sqrMagnitude > deadZone * deadZone) {
            transform.Translate(translation * speedModifier * accel * Time.deltaTime, transform.GetChild(0));
	    }

	    if(translation.sqrMagnitude > accelerationThreshold * accelerationThreshold) {
            accel *= 1f + accelerationRate * Time.deltaTime;
	    }
	    else {
	        accel = 1f;
	    }

		if(doRotate) {
			// This method keeps the horizon horizontal at all times.
			// Perform azimuth in world coordinates.
			transform.Rotate(Vector3.up, SpaceNavigator.Rotation.Yaw() * Mathf.Rad2Deg, Space.World);
			// Perform pitch in local coordinates.
			// transform.Rotate(Vector3.right, SpaceNavigator.Rotation.Pitch() * Mathf.Rad2Deg, Space.Self);
		}
	}
}
