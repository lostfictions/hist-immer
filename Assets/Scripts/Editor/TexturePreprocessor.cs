using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

using System.Linq;
using System.IO;

using UnityEditor;

public class TexturePostprocessor : AssetPostprocessor
{
    #region Preprocessor Methods

    void OnPreprocessTexture()
    {
        var pathsToPreActions = new Dictionary<string, Action<TextureImporter>> {
            { "Assets/Resources/Photos", PreprocessPhotos },
            { "Assets/Resources/Transparent", PreprocessPhotosTransparent },
        };

        var importer = (TextureImporter)assetImporter;

//        bool didPreprocess = false;
        foreach(var kvp in pathsToPreActions) {
            if(assetPath.StartsWith(kvp.Key, StringComparison.InvariantCultureIgnoreCase)) {
                kvp.Value(importer);
//                didPreprocess = true;
                break;
            }
        }

//        if(!didPreprocess) {
//            Debug.LogWarning("Didn't preprocess texture " + assetPath + " because it didn't match any preprocessing folders.");
//        }
    }

    void PreprocessPhotos(TextureImporter importer)
    {
//        importer.SetTextureSettings(new TextureImporterSettings {
//            spriteMeshType = SpriteMeshType.FullRect
//        });
        importer.textureType = TextureImporterType.Sprite;
        importer.maxTextureSize = 512;
        importer.spriteImportMode = SpriteImportMode.Single;
        importer.spritePackingTag = "photos";
        importer.alphaSource = TextureImporterAlphaSource.None; 
    }

    void PreprocessPhotosTransparent(TextureImporter importer)
    {
//        importer.SetTextureSettings(new TextureImporterSettings {
//            spriteMeshType = SpriteMeshType.FullRect
//        });
        importer.textureType = TextureImporterType.Sprite;
        importer.maxTextureSize = 512;
        importer.spriteImportMode = SpriteImportMode.Single;
        importer.spritePackingTag = "photos";
        importer.alphaSource = TextureImporterAlphaSource.FromGrayScale;
    }
    #endregion
}
